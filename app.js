const form = document.querySelector(".my-form");
const email = document.querySelector(".email");
const password = document.querySelector(".password");
const sex = document.querySelector(".sex");
const permissions = document.querySelector(".permissions");
const msg = document.querySelector(".msg");
const output = document.querySelector(".output");
const checkbox = document.forms[0].permission;
let checkCount = 0;

// Add submit event listner to form
form.addEventListener("submit", onSubmit);

function onSubmit(e) {
  // Prevent default behaviour of submit
  e.preventDefault();

  // Regex to validate password
  let re = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{6,20}$/;

  // Check count of permisions
  for (let i = 0; i < checkbox.length; i++) {
    if (checkbox[i].checked) {
      checkCount++;
    }
  }

  // Check if all the feilds are filled
  if (email.value == "" || password.value == "" || checkCount < 2) {
    msg.style.display = "Block";
    msg.textContent = "Please Fill all the feilds !!!";
    setTimeout(() => (msg.style.display = "None"), 2000);
  }

  // Validate the password
  else if (!password.value.match(re)) {
    msg.style.display = "Block";
    msg.textContent = "Password is not valid";
    setTimeout(() => (msg.style.display = "None"), 2000);
  }

  // Submit the form and display the output
  else {
    // display the output and disapper the form
    console.log("Everything is good");
    form.style.display = "None";
    output.style.display = "Block";

    // Outputting other credentials
    let child1 = document.createElement("h3");
    child1.innerText = `Email:${email.value} Password: ${password.value}`;
    let child2 = document.createElement("h3");
    child2.innerText = `Sex:${sex.value}`;

    output.appendChild(child1);
    output.appendChild(child2);

    // Outputting permissions
    for (let i = 0; i < checkbox.length; i++) {
      if (checkbox[i].checked) {
        console.log("I am in");
        let child3 = document.createElement("h3");
        child3.innerText = `Permissions:${checkbox[i].value}`;
        output.appendChild(child3);
      }
    }
  }
}
